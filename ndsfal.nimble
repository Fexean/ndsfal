# Package

version       = "0.1.0"
author        = "Fexean"
description   = "NDS file access logging tool"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["ndsfal"]


# Dependencies

requires "nim >= 2.0.2"
