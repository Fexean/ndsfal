
# NDSFAL - NDS File Access Logger
This is a Nintendo DS reverse engineering tool which tells you what files a game is accessing while it's running. It can help you figure out which file(s) contain the data you're interested in editing/extracting.

The tool connects to a GDB stub hosted on an emulator (such as melonDS), scans the console's memory (or if available, arm9.bin loaded from a ROM file) to find FS\_OpenFile, FS\_OpenFileFast and FSi\_ReadFileCore, and sets breakpoints on them.
On breakpoint hits the name of the accessed file is printed to stdout, alongside with the link register value and a timestamp.

## Limitations
The efficiency of the tool depends on the game. For some games the tool is unable to even find the file functions, and you won't be able to use the tool unless you find them manually. Other games might access files in ways not detected by this tool.
Even when the tool detects the file accesses, some games just keep all their data in arm9.bin or in massive archive files making this tool not very useful.

## Compiling
Given that you have installed [nim/nimble](https://nim-lang.org/), run `nimble install` to compile and install the project.

## Usage
Before running the program, start an NDS emulator and configure it to start a GDB stub for the ARM9 core. Note the port number the GDB stub is using.

Then start NDSFAL from the command line in the following manner:

    USAGE: ./ndsfal [options] <GDB port> [NDS ROM Path]
    OPTIONS:
    -d	scan memory dumped from RAM instead of reading arm9.bin from ROM
    --host=<address>	set GDB server hostname (default: localhost)
    --FS_OpenFile=<hex address>		manually specify the address for FS_OpenFile
    --FS_OpenFileFast=<hex address>		manually specify the address for FS_OpenFileFast
    --FSi_ReadFileCore=<hex address>	manually specify the address for FSi_ReadFileCore

    Note that you can prevent certain functions from being logged by setting their addresses to 0.


### Notes
- If a filepath starts with 3 letters followed by ':', the file is read from inside a narc file.
- At the time of writing this, melonDS builds don't include the GDB stub yet, so you must build the emulator from source.
- Sometimes the melonDS process stays alive after closing the emulator and it must be manually killed before the tool can connect again.
