import std/strutils
import macros

type
  SubPattern* = object
    offset: int
    bytes: seq[uint8]
    hash: int

type
  Signature* = object
    patterns* : seq[SubPattern]
    longestPatternIdx : int
    totalLen : int

proc sumHash(data: seq[uint8], low : int, high : int) : int =
  for i in low..high:
    result += int(data[i])

macro newSig*(strlit : typed): Signature =
  assert strlit.kind == nnkStrLit, "newSig parameter needs to be a string literal"
  let str : string = strlit.strVal 
  let bytecount = (str.len + 1) div 3
  var tuples : seq[SubPattern] = @[]
  var i = 0
  var longestSubIdx = 0
  while i < bytecount:
    while i < bytecount and str[i*3] == '?':
      i += 1
    if i >= bytecount:
      break
    
    var bytes : seq[uint8] = @[]
    var hash : int = 0
    for j in i .. bytecount-1:
      if str[j*3] == '?':
        break
      let b = parseHexInt(str.substr(j*3, j*3+1))
      hash += b
      bytes.add(uint8(b))
    tuples.add(SubPattern(offset: i, bytes: bytes, hash: sumHash(bytes, 0, bytes.high)))
    i += bytes.len
    longestSubIdx = if tuples[longestSubIdx].bytes.len > bytes.len: longestSubIdx else: tuples.high

  var totalLen = 0
  for pat in tuples:
    totalLen = max(totalLen, pat.offset + pat.bytes.len)
    
  quote do:
    Signature(patterns: @ `tuples`, longestPatternIdx: `longestSubIdx`, totalLen: `totalLen`)


proc cmpSeq(data: seq[uint8], address : int, pattern : seq[uint8]): bool =
  if address + pattern.len > data.high:
    return false
  for i in 0..pattern.len - 1:
    if pattern[i] != data[address+i]: return false
  return true

proc checkSignature*(data : seq[uint8], address : int, sign : Signature) : bool =
  for pat in sign.patterns:
    if not cmpSeq(data, address + pat.offset, pat.bytes):
      return false
  return true

proc findSignatureSlow*(data: seq[uint8], sign : Signature) : int =
  for i in 0 .. data.high - sign.totalLen:
    if checkSignature(data, i, sign):
      return i
  return -1

proc findSignature*(data: seq[uint8], sign : Signature) : int =
  let longest = sign.patterns[sign.longestPatternIdx]
  let lonlen = longest.bytes.len
  var hash = sumHash(data, longest.offset, lonlen + longest.offset - 1)
  for i in longest.offset .. data.high - sign.totalLen:
    if hash == longest.hash and checkSignature(data, i-longest.offset, sign):
        return i - longest.offset
    hash -= int(data[i])
    hash += int(data[i + lonlen])
  return -1


proc findOneOf*(memory : seq[uint8], signatures : openArray[Signature]) : int =
  for sign in signatures:
    let loc = findSignature(memory, sign)
    if loc != -1:
      return loc
  return -1

proc checkOneOf*(data: seq[uint8], address : int, signatures: openArray[Signature]) : int =
  if address <= 0:
    return -1
  for i in 0..signatures.high:
    if checkSignature(data, address, signatures[i]):
      return i
  return -1

