import signatures, signature_data, gdb as dbg
import fnt as ndsfnt
import std/strformat
import std/strutils
import std/times
import std/cmdline
import std/parseopt
import std/tables

const ANSI_CYAN = "\e[96m"
const ANSI_GREEN = "\e[92m"
const ANSI_YELLOW="\e[93m"

proc colorizeText(text : string, color : string) : string = color & text & "\e[0m"
var colorize = (proc (s,c : string): string = s)
proc timestamp() : string = colorize(now().format("'['HH:mm:ss'.'fff']'"), ANSI_GREEN)
proc formatHexAddr(num : Natural) : string = colorize(fmt"0x{num:06X}", ANSI_YELLOW)
proc formatNum(num : Natural) : string = colorize(fmt"0x{num:04X}", ANSI_YELLOW)
proc formatPath(path : string) : string = colorize(fmt"'{path}'", ANSI_CYAN)

proc printHelp() =
  echo fmt"USAGE: {paramStr(0)} [options] <GDB port> [NDS ROM Path]"
  echo "OPTIONS:"
  echo "-d\tscan memory dumped from RAM instead of reading arm9.bin from ROM"
  echo "-c\tcolorize the terminal output"
  echo "--host=<address>\tset GDB server hostname (default: localhost)"
  echo "--FS_OpenFile=<hex address>\t\tmanually specify the address for FS_OpenFile"
  echo "--FS_OpenFileFast=<hex address>\t\tmanually specify the address for FS_OpenFileFast"
  echo "--FSi_ReadFileCore=<hex address>\tmanually specify the address for FSi_ReadFileCore"
  echo "\nNote that you can prevent certain functions from being logged by setting their addresses to 0."

type 
  CliOpt* = object
    error*: bool
    port*: int
    rompath*: string
    values*: Table[string, string]
    flags*: Table[string, bool]

proc parseOptions(): CliOpt =
  var p = initOptParser(commandLineParams())
  var args : seq[string]
  for kind, key, val in p.getopt():
    case kind
    of cmdArgument:
      args.add(key)
    of cmdShortOption, cmdLongOption:
      if val == "":
        result.flags[key] = true
      else:
        result.values[key] = val
    else: discard
  if args.len == 0 or args.len > 2:
    result.error = true
    return
  if args.len == 2:
    result.rompath = args[1] 
  try:
    result.port = parseInt(args[0])
  except:
    echo "Expected port number, got:", args[0]
    result.error = true


proc setBreakpointOnFunc(gdb : var Gdb, memory : seq[uint8],  name : string, signatures : openArray[Signature], opt: CliOpt) : int =
  if opt.values.hasKey(name):
    result = parseHexInt(opt.values[name])
  else:
    let loc = findOneOf(memory, signatures)
    if loc != -1:
      result = 0x2000000 + loc
      echo fmt"Found {name} at 0x{result:08X}"
    else:
      echo "Unable to find ", name
      return -1

  if result <= 0:
    echo "Ignoring ", name
  elif not gdb.setBreakpoint(result):
    gdb.detach()
    quit(fmt"Failed to set breakpoint on {name} ({result})")


proc onFsOpenFile(gdb : Gdb, regs : regArray, fnt : Table[int, string]) =
  echo fmt"{timestamp()} FS_OpenFile(file={formatHexAddr(regs[0])}, {formatPath(gdb.readAscii(int(regs[1])))}) lr={regs[14]:06x}"

proc onFsOpenFileFast(gdb : Gdb, regs : regArray, fnt : Table[int, string]) =
  let fileId = int(regs[2])
  let narcName = gdb.readAscii(int(regs[1]), 3)
  if narcName == "rom": # not a narc
    let filename = if fnt.hasKey(fileId): formatPath(fnt[fileId]) else : fmt"FILE {formatNum(fileId)}"
    echo fmt"{timestamp()} FS_OpenFileFast(file={formatHexAddr(regs[0])}, {filename}) lr={regs[14]:06x}"
  else:
    echo fmt"{timestamp()} FS_OpenFileFast(file={formatHexAddr(regs[0])}, {narcName}):FILE {formatNum(fileId)}) lr={regs[14]:06x}"

proc onFsReadFile(gdb : Gdb, regs : regArray, readFileSignId : int) =
  let filePropOffset = if readFileSignId < numReadFile0x28Sigs: 0x1C else: 0x20
  if readFileSignId == -1: # don't know the offset for fileProps and can't determine file position
    echo fmt"{timestamp()} FSi_ReadFileCore(file={formatHexAddr(regs[0])}, dst={formatHexAddr(regs[1])}, len={formatNum(regs[2])}) lr={regs[14]:06x}"
  else:
    let fileProps = int(regs[0]) + filePropOffset 
    let filePos = gdb.readWord(fileProps + 12) - gdb.readWord(fileProps + 4)
    echo fmt"{timestamp()} FSi_ReadFileCore(file={formatHexAddr(regs[0])}, src={formatNum(filePos)}, dst={formatHexAddr(regs[1])}, len={formatNum(regs[2])}) lr={regs[14]:06x}"

let opt = parseOptions()
if opt.error or opt.flags.getOrDefault("help") or opt.flags.getOrDefault("h"):
  printHelp()
  quit()

if opt.flags.getOrDefault("c"):
  colorize = colorizeText

var mem : seq[uint8]
var fnt : Table[int, string]
if opt.rompath != "":
  echo "Reading file names from ROM..."
  fnt = readFntFromFile(opt.rompath)
  if not opt.flags.getOrDefault("d"):
    echo "Reading arm9.bin from ROM..."
    mem = readArm9FromFile(opt.rompath)
else:
  echo "No ROM provided, file names will be unavailable for FS_OpenFileFast."


let host = opt.values.getOrDefault("host", "localhost")
echo fmt"Connecting to GDB stub ({host}:{opt.port})..."
var gdb : Gdb = connectToGdb(host, opt.port)

proc ctrlc() {.noconv.} =
  gdb.detach()
  quit("Exited!")
setControlCHook(ctrlc)

discard gdb.sendSupported()

if mem.len == 0:
  echo "Dumping emulator memory..."
  mem = gdb.readMemoryInBlocks(0x2000000, 4*1024*1024)

echo "Searching for functions..."
let addrFsOpenFile = gdb.setBreakpointOnFunc(mem, "FS_OpenFile", signatures_FsOpenFile, opt)
let addrFsOpenFileFast = gdb.setBreakpointOnFunc(mem, "FS_OpenFileFast", signatures_FsOpenFileFast, opt)
let addrFsReadFile = gdb.setBreakpointOnFunc(mem, "FSi_ReadFileCore", signatures_FSiReadFileCore, opt)

let readFileSignId = checkOneOf(mem, addrFsReadFile-0x2000000, signatures_FSiReadFileCore)

if addrFsOpenFile == -1 and addrFsOpenFileFast == -1 and addrFsReadFile == -1:
  gdb.detach()
  quit("No functions to log, exiting.")

gdb.gdbContinue()
while true:
  var cmd = recvPacket(gdb, 0, 100000000)
  if (cmd[0] == 'T' and cmd.substr(5, 9) == "break") or cmd == "S05":
    let regs = gdb.readRegisters()
    let pc = int(regs[15])
    if pc == addrFsReadFile:
      onFsReadFile(gdb, regs, readFileSignId)
    elif pc == addrFsOpenFile:
      onFsOpenFile(gdb, regs, fnt)
    elif pc == addrFsOpenFileFast and abs(int(regs[14]) - addrFsOpenFile) > 50:
      onFsOpenFileFast(gdb, regs, fnt)

  gdb.gdbContinue()
