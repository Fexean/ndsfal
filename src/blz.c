/*----------------------------------------------------------------------------*/
/*--  blz.c - Bottom LZ coding for Nintendo GBA/DS                          --*/
/*--  Copyright (C) 2011 CUE                                                --*/
/*--                                                                        --*/
/*--  This program is free software: you can redistribute it and/or modify  --*/
/*--  it under the terms of the GNU General Public License as published by  --*/
/*--  the Free Software Foundation, either version 3 of the License, or     --*/
/*--  (at your option) any later version.                                   --*/
/*--                                                                        --*/
/*--  This program is distributed in the hope that it will be useful,       --*/
/*--  but WITHOUT ANY WARRANTY; without even the implied warranty of        --*/
/*--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          --*/
/*--  GNU General Public License for more details.                          --*/
/*--                                                                        --*/
/*--  You should have received a copy of the GNU General Public License     --*/
/*--  along with this program. If not, see <http://www.gnu.org/licenses/>.  --*/
/*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>


#define CMD_DECODE 0x00 // decode
#define CMD_ENCODE 0x01 // encode

#define BLZ_NORMAL 0 // normal mode
#define BLZ_BEST   1 // best mode

#define BLZ_SHIFT 1 // bits to shift
#define BLZ_MASK \
    0x80 // bits to check:
         // ((((1 << BLZ_SHIFT) - 1) << (8 - BLZ_SHIFT)

#define BLZ_THRESHOLD 2      // max number of bytes to not encode
#define BLZ_N         0x1002 // max offset ((1 << 12) + 2)
#define BLZ_F         0x12   // max coded ((1 << 4) + BLZ_THRESHOLD)

#define RAW_MINIM 0x00000000 // empty file, 0 bytes
#define RAW_MAXIM 0x00FFFFFF // 3-bytes length, 16MB - 1

#define BLZ_MINIM 0x00000004 // header only (empty RAW file)
#define BLZ_MAXIM \
    0x01400000 // 0x0120000A, padded to 20MB:
               // * length, RAW_MAXIM
               // * flags, (RAW_MAXIM + 7) / 8
               // * header, 11
               // 0x00FFFFFF + 0x00200000 + 12 + padding







void BLZ_Invert(unsigned char *buffer, size_t length)
{
    unsigned char *bottom = buffer + length - 1;

    while (buffer < bottom)
    {
        unsigned char ch = *buffer;
        *buffer++ = *bottom;
        *bottom-- = ch;
    }
}


static const char* gBLZ_error;

const char* BLZ_GetError(){
	return gBLZ_error;
}

void BLZ_SetError(const char* message){
	gBLZ_error = message;
}

#define EXIT(text)    \
    {                 \
        BLZ_SetError(text); \
        return NULL;     \
    }

unsigned char* BLZ_Decode(unsigned char *pak_buffer, size_t pak_len, size_t* len_out)
{
    unsigned char *raw_buffer, *pak, *raw, *pak_end, *raw_end;

    unsigned int raw_len, len, pos, inc_len, hdr_len, enc_len, dec_len;
    unsigned char flags, mask;
	
	*len_out = 0;
    inc_len = *(unsigned int *)(pak_buffer + pak_len - 4);
    if (!inc_len)
    {
        BLZ_SetError("WARNING: not coded file!");
        enc_len = 0;
        dec_len = pak_len;
        pak_len = 0;
        raw_len = dec_len;
    }
    else
    {
        if (pak_len < 8)
            EXIT("File has a bad header");
        hdr_len = pak_buffer[pak_len - 5];
        if ((hdr_len < 0x08) || (hdr_len > 0x0B))
            EXIT("Bad header length");
        if (pak_len <= hdr_len)
            EXIT("Bad length");
        enc_len = *(unsigned int *)(pak_buffer + pak_len - 8) & 0x00FFFFFF;
        dec_len = pak_len - enc_len;
        pak_len = enc_len - hdr_len;
        raw_len = dec_len + enc_len + inc_len;
        if (raw_len > RAW_MAXIM)
            EXIT("Bad decoded length");
    }

    raw_buffer = calloc(raw_len, sizeof(char));
    if(!raw_buffer){
    	return NULL;
    }

    pak = pak_buffer;
    raw = raw_buffer;
    pak_end = pak_buffer + dec_len + pak_len;
    raw_end = raw_buffer + raw_len;

    for (len = 0; len < dec_len; len++)
        *raw++ = *pak++;

    BLZ_Invert(pak_buffer + dec_len, pak_len);

    flags = 0;
    mask = 0;

    while (raw < raw_end)
    {
        if (!(mask >>= BLZ_SHIFT))
        {
            if (pak == pak_end)
                break;
            flags = *pak++;
            mask = BLZ_MASK;
        }

        if (!(flags & mask))
        {
            if (pak == pak_end)
                break;
            *raw++ = *pak++;
        }
        else
        {
            if (pak + 1 >= pak_end)
                break;
            pos = *pak++ << 8;
            pos |= *pak++;
            len = (pos >> 12) + BLZ_THRESHOLD + 1;
            if (raw + len > raw_end)
            {
                BLZ_SetError("WARNING: wrong decoded length!");
                len = raw_end - raw;
            }
            pos = (pos & 0xFFF) + 3;
            while (len--)
            {
                *raw = *(raw - pos);
                raw++;
            }
        }
    }

    BLZ_Invert(raw_buffer + dec_len, raw_len - dec_len);

    raw_len = raw - raw_buffer;

    if (raw != raw_end)
        BLZ_SetError("WARNING: unexpected end of encoded file!");
	else
    	BLZ_SetError("");
    	
    *len_out = raw_len;
    return raw_buffer;
}

