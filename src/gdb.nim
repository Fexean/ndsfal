import std/strformat
import std/net
import std/strutils
import std/bitops
from std/private/decode_helpers import handleHexChar

type
  Gdb* = object
    sock* : Socket
  regArray* = array[0 .. 16, uint32]

proc connectToGdb*(address : string, port : int) : Gdb =
  result = Gdb(sock: newSocket())
  result.sock.connect(address, Port(port), 5000)
  result.sock.send("+")

proc packetChecksum(data : string) : uint8 =
  result = 0
  for i in 0 .. data.len-1:
    result += cast[uint8](data[i])

proc sendPacket*(gdb : Gdb, data : string) : bool =
  let packet = fmt"${data}#{packetChecksum(data):02x}"
  var response : string
  for retries in 1 .. 10:
    gdb.sock.send(packet)
    discard gdb.sock.recv(response, 1, timeout = 5000)
    if response == "+":
      return true
    if response == "-":
      echo "resending gdb packet..."
      continue
    break
  echo "Failed to send:", packet, "response:",response
  return false


proc recvPacket*(gdb : Gdb, expectedLen : int = 0, timeout = 5000) : string =
  var recvBuf : string
  var cmd : string = newStringOfCap(1024)
  if expectedLen > 0:
    discard gdb.sock.recv(cmd, expectedLen, timeout)
  while cmd.len < 4 or cmd[cmd.len-3] != '#':
    discard gdb.sock.recv(recvBuf, 1, timeout)
    cmd.add(recvBuf)
  if cmd[0] == '+': # ack before packet
    result = cmd.substr(2, cmd.len-4)
  else:
    result = cmd.substr(1, cmd.len-4)
  gdb.sock.send("+")

proc detach*(gdb : Gdb) =
  discard gdb.sendPacket("D")
  gdb.sock.close()

proc sendSupported*(gdb : Gdb) : string =
  let cmd = "qSupported:swbreak+;hwbreak+"
  result = ""
  if gdb.sendPacket(cmd):
    result = recvPacket(gdb)

proc swapEndianness(x : uint32) : uint32 =
  bitor(bitand(x, 0xff) shl 24,
        bitand(x, 0xff00) shl 8,
        bitand(x, 0xff0000) shr 8,
        x shr 24)

proc gdbContinue*(gdb : Gdb) = discard gdb.sendPacket("c")

proc readRegisters*(gdb : Gdb) : regArray =
  if gdb.sendPacket("g"):
    var resp = gdb.recvPacket(result.len*8 + 4)
    for r in 0 .. 16:
      result[r] = swapEndianness(uint32(parseHexInt(resp.substr(8*r, 8*r+7))))


proc readHexByteFromStr(str : string, pos : int) : uint8 =
  uint8(handleHexChar(str[pos]) * 16) + uint8(handleHexChar(str[pos+1]))

proc readMemory(gdb : Gdb, address : int, size : int) : seq[uint8] =
  let cmd : string = fmt"m{address:x},{size:x}"
  result = newSeq[uint8](size)
  if gdb.sendPacket(cmd):
    var bytes = gdb.recvPacket(size*2 + 4)
    for i in 0 .. size-1:
      result[i] = readHexByteFromStr(bytes, 2*i)

proc readWord*(gdb : Gdb, address : int) : uint32 =
  let data = gdb.readMemory(address, 4)
  result = bitor(uint32(data[0]),
                 uint32(data[1]) shl 8,
                 uint32(data[2]) shl 16,
                 uint32(data[3]) shl 24)

proc readMemoryInBlocks*(gdb : Gdb, address : int, size : int) : seq[uint8] =
  var i = 0
  result = newSeq[uint8](size)
  while i < size:
    var blockk = gdb.readMemory(address + i, min(512, size-i))
    for j in 0 .. blockk.len-1:
      result[i+j] = blockk[j]
    i += blockk.len

proc setBreakpoint*(gdb : Gdb, address : int) : bool =
  let cmd = fmt"Z0,{address:x},0"
  result = gdb.sendPacket(cmd)
  if result:
    var r = gdb.recvPacket()
    result = "OK" in r

proc readAscii*(gdb : Gdb, address : int, maxSize : int = 128) : string = 
  let data = gdb.readMemory(address, maxSize)
  for i in 0 .. data.len-1:
    if data[i] == 0:
      break
    result.add(char(data[i]))

