import std/bitops
import std/tables
import std/strutils


{.compile: "blz.c".}
proc BLZ_Decode(data: ptr uint8, lenIn: csize_t, lenOut: ptr csize_t): ptr [uint8] {.importc.}
proc BLZ_GetError(): cstring {.importc.}
proc free(data: ptr uint8) {.importc.}

proc decompressBlz*(data : seq[uint8], address : int) : seq[uint8] =
  let length = csize_t(data.len - address)
  var decodedSize : csize_t
  var decoded = BLZ_Decode(addr data[address], length, addr decodedSize)
  if decodedSize == 0:
    let error : string = $BLZ_GetError()
    raise newException(ValueError, "BLZ_Decode failed to decompress data: " & error)
  result = newSeq[uint8](decodedSize)
  copyMem(addr result[0], decoded, decodedSize)
  free(decoded) 
  
proc readWord(data : seq[uint8], offset : int) : uint32 =
  bitor(uint32(data[offset+0]),
        uint32(data[offset+1]) shl 8,
        uint32(data[offset+2]) shl 16,
        uint32(data[offset+3]) shl 24)

proc read16(data : seq[uint8], offset : int) : uint16 =
  bitor(uint16(data[offset+0]),
        uint16(data[offset+1]) shl 8)

proc readFntRecursive(memory : seq[uint8], start : int, dir_id : int, dirName : string, fntTable : var Table[int, string]) = 
  let tbl = start + 8*(dir_id and 0xfff)
  var entryStart = int(memory.readWord(tbl))
  let parentId = int(memory.read16(tbl + 6))
  var fileId = int(memory.read16(tbl + 4))
  while true:
    let nameLen = int(memory[entryStart + start] and 0x7f)
    if nameLen == 0:
      break
    let isDir = memory[entryStart + start] > 0x80
    var name : string
    for j in 1 .. nameLen:
      name.add(char(memory[entryStart + start + j]))

    if isDir:
      let dirId2 = memory.read16(entryStart + start + nameLen + 1)
      readFntRecursive(memory, start, int(dirId2), dirName & name & "/", fntTable)
      entryStart += 2
    else:
      fntTable[fileId] = dirname & name

    entryStart += (nameLen + 1)
    fileId += 1

proc readFnt(memory : seq[uint8], fntStart : int) : Table[int, string] = 
  readFntRecursive(memory, fntStart, 0xf000, "/", result)


proc readWord(file : var File, offset : int) : int =
  var buf = newSeq[uint8](4)
  file.setFilePos(offset)
  discard file.readBytes(buf, 0, 4)
  return int(buf.readWord(0))

proc readDataFromPtrSizeTupleAt(file : var File, dataOffset : int, sizeOffset : int) : tuple[address : int, data : seq[uint8]] =
  let size = file.readWord(sizeOffset)
  let start = file.readWord(dataOffset)
  var buf = newSeq[uint8](size)
  if size == 0:
    return (start, buf)
  file.setFilePos(start)
  discard file.readBytes(buf, 0, size)
  return (start, buf)

proc addOverlaysToTable(file : var File, fnt : var Table[int, string]) =
  let overlayTable = readDataFromPtrSizeTupleAt(file, 0x50, 0x54)
  let overlayCount = overlayTable.data.len div 32
  file.setFilePos(overlayTable.address)
  var buf = newSeq[uint8](32)
  for i in 0..overlayCount-1:
    discard file.readBytes(buf, 0, 32)
    let ovid = int(buf.readWord(0))
    let fileId = int(buf.readWord(0x18))
    fnt[fileId] = "OVERLAY " & intToStr(ovid)

proc readFntFromFile*(path : string) : Table[int, string] = 
  var file = open(path, fmRead)
  let fntData = readDataFromPtrSizeTupleAt(file, 0x40, 0x44).data
  var fnt = readFnt(fntData, 0)
  addOverlaysToTable(file, fnt)
  file.close()
  return fnt

proc readArm9FromFile*(path : string) : seq[uint8] =
  var file = open(path, fmRead)
  let load = file.readWord(0x28)
  let auto = file.readWord(0x70)
  let arm9 = readDataFromPtrSizeTupleAt(file, 0x20, 0x2C).data
  file.close()
  if auto < load+4:
    return arm9
  let offset = int(arm9.readWord(auto - 4 - load)) - load
  let isCompressed = offset < arm9.high and arm9.readWord(offset + 20) != 0
  result = if isCompressed: decompressBlz(arm9, 0) else: arm9

